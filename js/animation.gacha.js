"use strict";
var Gacha = function(){
	var self = this;
	var dfd = $.Deferred();
	var dfdPromise = dfd.promise();
	this.setup = function(){
		// ステージ全体
		var stage = new cb.CBLayer();
		stage.setSize({width:320,height:480});
		this.elem = stage.elem;
		$(this).find(".screen").append(stage.elem);
		// ガチャ本体
		var gacha = new cb.CBSprite();
		gacha.setElement($(".asset[data-name='gacha-machine']"));
		gacha.setPosition({x:160,y:240});
		stage.addChild(gacha);
		// カプセル
		var capsule = new cb.Effect();
		capsule.setup("gacha-capsule");
		capsule.setPosition({x:160,y:310});
		stage.addChild(capsule);
		// 中身
		var content = new cb.Effect();
		content.setup("gacha-content");
		content.setPosition({x:160,y:240});
		stage.addChild(content);
		// info
		var info = new cb.Effect();
		info.setup("gacha-status-info");
		info.setPosition({x:160,y:60});
		stage.addChild(info);
		
		dfdPromise = dfdPromise.pipe(function(){
			return capsule.invoke(true);
		}).pipe(function(){
			info.invoke(true)
			return content.invoke(true);
		});
	}
	// 再生スタート
	this.play = function(){
		dfd.resolve();
		return dfdPromise;
	}
}