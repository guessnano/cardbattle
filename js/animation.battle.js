"use strict";
var Battle = function(){
	var self = this;
	var dfd = $.Deferred();
	var dfdPromise = dfd.promise();
	this.setup = function(obj){
		// モンスター配置
		var monsterPosition = {};
		monsterPosition.players=[];
		monsterPosition.players[3] = {x:4,y:229};
		monsterPosition.players[1] = {x:67,y:229};
		monsterPosition.players[0] = {x:130,y:229};
		monsterPosition.players[2] = {x:193,y:229};
		monsterPosition.players[4] = {x:256,y:229};
		monsterPosition.players[5] = {x:306,y:229};
		monsterPosition.enemies = [];
		monsterPosition.enemies[3] = {x:4,y:31};
		monsterPosition.enemies[1] = {x:67,y:31};
		monsterPosition.enemies[0] = {x:130,y:31};
		monsterPosition.enemies[2] = {x:193,y:31};
		monsterPosition.enemies[4] = {x:256,y:31};
		monsterPosition.enemies[5] = {x:306,y:31};
		
		// ステージ全体
		var stage = new cb.CBLayer();
		stage.setSize({width:320,height:440});
		this.elem = stage.elem;
		// 背景
		var field = self.field = new cb.CBSprite();
		field.setSize({width:320,height:440});
		field.elem.addClass("field");
		stage.addChild(field);
		// 味方モンスター
		var friendForce = {};
		for(var i=0;i<obj.friendForce.length;i++){
			friendForce[i] = new cb.Monster();
			friendForce[i].setup("friend-"+(i+1));
			stage.addChild(friendForce[i]);
			friendForce[i].setPosition(monsterPosition.players[i]);
		}
		// 敵モンスター
		var enemyForce = {};
		for(var i=0;i<obj.enemyForce.length;i++){
			enemyForce[i] = new cb.Monster();
			enemyForce[i].setup("friend-"+(i+1));
			stage.addChild(enemyForce[i]);
			enemyForce[i].setPosition(monsterPosition.enemies[i]);
		}
		// intro
		dfdPromise = dfdPromise.pipe(function(){
			
		}).pipe(function(){
			return cb.waitFor(500);
		}).pipe(function(){
		});
		return this;
	}
	this.play = function(){
		dfd.resolve();
		return dfdPromise;
	}
}