<div data-role="page" id="battle-scene-page" data-theme="j">
	<div class="screen"></div>
	<!--<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>-->
</div>
<div class="assets">
        <div data-role="monster-asset" data-name="friend-1" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy001.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy001.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="friend-2" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy002.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy002.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="friend-3" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy003.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy003.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="friend-4" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy004.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy004.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="friend-5" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy005.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy005.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="friend-6" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy006.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy006.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="enemy-1" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy011.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy011.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="enemy-2" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy012.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy012.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="enemy-3" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy013.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy013.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="enemy-4" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy014.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy014.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="enemy-5" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy015.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy015.png" width="120" height="120" class="monster" />
        </div>
        <div data-role="monster-asset" data-name="enemy-6" class="asset" style="width:120px; height:120px" data-src="./img/monsters/pipo-enemy016.png" data-hit-point="100" data-max-hit-point="100" >
            <img src="./img/monsters/pipo-enemy016.png" width="120" height="120" class="monster" />
        </div>
</div>
<script>
	var battlePage = {};
	battlePage.settings = {};
	battlePage.settings.friendForce = [{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}];
	battlePage.settings.enemyForce = [{id:1},{id:2},{id:3},{id:4},{id:5},{id:6}];
</script>