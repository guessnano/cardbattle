<div data-role="page" id="battle-list-page" data-theme="j">
	<div class="content">
		<div class="tab-holder">
			<ul class="tab-bar">
				<li class="tab selected"><a href="#battle-list-page"><div class="tab-content">タブ1</div></a></li>
				<li class="tab"><a href="#battle-list-similar-level-page"><div class="tab-content">タブ2</div></a></li>
				<li class="tab"><a href="#battle-list-rival-page"><div class="tab-content">タブ3</div></a></li>
			</ul>
		</div>
		<ul class="box-list-holder">
		<li class="">
			<a href="" data-href-id="#battle-scene-page" data-param="opponent_id=7">
				<div class="opponent-thumb"></div>
				<div class="opponent-status">
					<div class="opponent-name">対戦者7</div>
				</div>
			</a>
		</li>
		<li class="">
			<a href="" data-href-id="#battle-scene-page" data-param="opponent_id=7">
				<div class="opponent-thumb"></div>
				<div class="opponent-status">
					<div class="opponent-name">対戦者7</div>
				</div>
			</a>
		</li>
		<li class="">
			<a href="" data-href-id="#battle-scene-page" data-param="opponent_id=7">
				<div class="opponent-thumb"></div>
				<div class="opponent-status">
					<div class="opponent-name">対戦者7</div>
				</div>
			</a>
		</li>
		<li class="">
			<a href="" data-href-id="#battle-scene-page" data-param="opponent_id=7">
				<div class="opponent-thumb"></div>
				<div class="opponent-status">
					<div class="opponent-name">対戦者7</div>
				</div>
			</a>
		</li>
		<li class="">
			<a href="" data-href-id="#battle-scene-page" data-param="opponent_id=7">
				<div class="opponent-thumb"></div>
				<div class="opponent-status">
					<div class="opponent-name">対戦者7</div>
				</div>
			</a>
		</li>
		<li class="">
			<a href="" data-href-id="#battle-scene-page" data-param="opponent_id=7">
				<div class="opponent-thumb"></div>
				<div class="opponent-status">
					<div class="opponent-name">対戦者7</div>
				</div>
			</a>
		</li>
		<li class="">
			<a href="" data-href-id="#battle-scene-page" data-param="opponent_id=7">
				<div class="opponent-thumb"></div>
				<div class="opponent-status">
					<div class="opponent-name">対戦者7</div>
				</div>
			</a>
		</li>
		</ul>
	</div>
	<footer>
		<ul>
			<li><a href="#home-page">HOME</a></li>
			<li><a href="#fusion-list-page">合成</a></li>
			<li><a href="#gacha-list-page">ガチャ</a></li>
			<li><a href="#menu-page">メニュー</a></li>
			<li><a href="#notification-list-page">通知</a></li>
		</ul>
	</footer>
</div>