<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,target-densitydpi=160" id="viewport" />
<meta name="format-detection" content="telephone=no" />

<!--<link rel="stylesheet" href="./css/jquery.mobile.custom.structure.min.css" />-->
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile.structure-1.2.0.css" />

<link rel="stylesheet/less" type="text/css" href="./css/reset.less" />
<link rel="stylesheet/less" type="text/css" href="./css/base.less" />
<link rel="stylesheet/less" type="text/css" href="./css/cb.keyframes.less" />
<link rel="stylesheet/less" type="text/css" href="./css/cb.base.less" />
<link rel="stylesheet/less" type="text/css" href="./css/cb.battle.less" />
<link rel="stylesheet/less" type="text/css" href="./css/main.less" />
<script>
var less = {
    env: 'development'
};
</script>
<script src="./js/less-1.3.1.min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script>
	less.watch();
	/* this section must be distinguished before loading jquery mobile */
	$(document).one("mobileinit", function(){
		$.mobile.defaultPageTransition = "none";
		$.mobile.defaultDialogTransition = "pop";
		$.event.special.swipe.horizontalDistanceThreshold = "60px";
	});
</script>
<!--<script src="./js/jquery.mobile.custom.min.js"></script>-->
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<title>CardGame Sample</title>
</head>
<body>
<?php include_once("home.php"); ?>
<?php include_once("quest-region-list.php"); ?>
<?php include_once("quest-area-list.php"); ?>
<?php include_once("quest-block-list.php"); ?>
<?php include_once("quest-scene.php"); ?>
<?php include_once("fusion-list.php"); ?>
<?php include_once("fusion-scene.php"); ?>
<?php include_once("gacha-list.php"); ?>
<?php include_once("gacha-scene.php"); ?>
<?php include_once("battle-list.php"); ?>
<?php include_once("battle-scene.php"); ?>
<script src="./js/cb.base.js"></script>
<script src="./js/cb.battle.js"></script>
<script src="./js/animation.battle.js"></script>
<script src="./js/animation.gacha.js"></script>
<script src="./js/main.js"></script>
<script>
	cb.initialize();
</script>
</body>
</html>